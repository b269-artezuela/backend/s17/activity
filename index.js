/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printUserDetails (){
	let fullname = prompt("Enter your fullname: ")
	let age = prompt("Enter your age: ")
	let location = prompt("Enter your location: ")
	console.log	("Welcome " + fullname)
	console.log	("You are " + age + " years old.")
	console.log	("You live in " + location)

}

printUserDetails();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function showFavoriteMusicArtists(){
	// Function scoped variables
	
	let favorite1 = "1.Parokya ni Edgar";
	let favorite2 = "2.Eraserheads";
	let favorite3 = "3.Spongecola";
	let favorite4 = "4.Maroon 5";
	let favorite5 = "5.Silent Sanctuary";

	console.log(favorite1);
	console.log(favorite2);
	console.log(favorite3);
	console.log(favorite4);
	console.log(favorite5);
	
}

showFavoriteMusicArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function showFavoriteMoviesWithRating(){
	// Function scoped variables
	
	let Movie1 = "1.Avengers";
	let Movie2 = "2.Avengers: Age of Ultron";
	let Movie3 = "3.Avengers: Infinity War";
	let Movie4 = "4.Avengers: End Game";
	let Movie5 = "5.Spiderman: No way home";

	console.log(Movie1);
	console.log("Rotten Tomatoes Rating: 91%")
	console.log(Movie2);
	console.log("Rotten Tomatoes Rating: 92%")
	console.log(Movie3);
	console.log("Rotten Tomatoes Rating: 93%")
	console.log(Movie4);
	console.log("Rotten Tomatoes Rating: 94%")
	console.log(Movie5);
	console.log("Rotten Tomatoes Rating: 95%")
	
}

showFavoriteMoviesWithRating();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/



// printUsers();
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();



// console.log(friend1);
// console.log(friend2);
